using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.Library.Domain;

public class Director : Entity<Guid>
{
    public string FirstName { get; private set; }
    public string LastName { get; private set; }

    [Obsolete("Only for EF", true)]
    private Director()
    {
    }

    public Director(string firstName, string lastName)
    {
        FirstName = firstName;
        LastName = lastName;
    }
}