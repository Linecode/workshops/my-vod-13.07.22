using EnsureThat;
using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.Library.Domain;

// public class Title : ValueObject<Title>
// {
//     public string Original { get; private set; }
//     public string English { get; private set; }
//
//     private Title(string original, string english)
//     {
//         Original = original;
//         English = english;
//     }
//
//     protected override IEnumerable<object> GetEqualityComponents()
//     {
//         yield return Original;
//         yield return English;
//     }
// }

// public class Titles : ValueObject<Titles>
// {
//     private List<Title> _all = new();
//     public IReadOnlyList<Title> All => _all.AsReadOnly();
//     public Title Default { get; private set; }
//
//     public Titles()
//     {
//         
//     }
//     
//     protected override IEnumerable<object> GetEqualityComponents()
//     {
//         yield return All;
//         yield return Default;
//     }
// }
//
// public class Title : ValueObject<Title>
// {
//     public string Value { get; private set; }
//     public string Lang { get; private set; }
//
//     public Title(string value, string lang)
//     {
//         Value = value;
//         Lang = lang;
//     }
//
//     protected override IEnumerable<object> GetEqualityComponents()
//     {
//         yield return Value;
//         yield return Lang;
//     }
// }

public class Title : ValueObject<Title>
{
    public string Original { get; private set; }
    public string English { get; private set; }

    [Obsolete("Only For EF", true)]
    private Title()
    { }
    
    public Title(string originalTitle, string englishTitle)
    {
        Ensure.That(originalTitle).HasLengthBetween(2, 100);
        Ensure.That(englishTitle).HasLengthBetween(2, 100);
        
        Original = originalTitle;
        English = englishTitle;
    }
    
    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Original;
        yield return English;
    }
}