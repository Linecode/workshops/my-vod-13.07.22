using Microsoft.EntityFrameworkCore;
using MyVod.Domain.Library.Domain;
using MyVod.Domain.Library.Infrastructure;
using MyVod.Domain.SharedKernel;

namespace MyVod.Infrastructure.Library;

public class MoviesRepository : IMovieRepository
{
    private readonly LibraryDbContext _context;

    public Common.BuildingBlocks.EfCore.IUnitOfWork UnitOfWork => _context;

    public MoviesRepository(LibraryDbContext context)
    {
        _context = context;
    }

    public Task<Movie?> Get(MovieId id)
    {
        return _context.Movies
            .Include(x => x.Director)
            .SingleOrDefaultAsync(x => x.Id == id);
    }
}