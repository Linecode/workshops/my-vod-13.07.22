using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyVod.Domain.Library.Domain;

namespace MyVod.Infrastructure.Library;

public class DirectorEntityTypeConfiguration : IEntityTypeConfiguration<Director>
{
    public void Configure(EntityTypeBuilder<Director> builder)
    {
        builder.ToTable("Persons");

        builder.HasKey(x => x.Id);
        builder.Property(x => x.FirstName);
        builder.Property(x => x.LastName);

        builder.Ignore(x => x.DomainEvents);
    }
}