using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyVod.Domain.Library.Domain;
using MyVod.Domain.SharedKernel;

namespace MyVod.Infrastructure.Library;

public class MovieEntityTypeConfiguration : IEntityTypeConfiguration<Movie>
{
    public void Configure(EntityTypeBuilder<Movie> builder)
    {
        builder.ToTable("Movies");

        builder.HasKey(x => x.Id);

        builder.Property(x => x.Id)
            .HasConversion(o => o.Value, s => MovieId.Parse(s));

        builder.OwnsOne(x => x.Title, t =>
        {
            t.Property(x => x.English)
                .HasColumnName("EnglishTitle");

            t.Property(x => x.Original)
                .HasColumnName("Title");
        });

        builder.HasOne(x => x.Director);
        
        builder.Ignore(x => x.DomainEvents);
    }
}