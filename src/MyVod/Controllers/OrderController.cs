using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyVod.Infrastructure.Models;
using MyVod.Services;
using Stripe;
using Stripe.Checkout;
using Order = MyVod.Infrastructure.Models.Order;

namespace MyVod.Controllers;

public class OrderController : Controller
{
    private readonly IMovieService _movieService;
    private readonly IOrderService _orderService;

    public OrderController(IMovieService movieService, IOrderService orderService)
    {
        _movieService = movieService;
        _orderService = orderService;
    }
    
    [Authorize]
    [HttpPost("/order/{movieId:guid}")]
    public async Task<IActionResult> Index([FromRoute]Guid movieId, [FromForm]Order order)
    {
        order.MovieId = movieId;
        var movie = await _movieService.Get(movieId);
        order.MoviePrice = movie.Price;
        order.TaxRate = 23;
        order.Id = Guid.NewGuid();
        order.UserId = Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

        await _orderService.Create(order);

        var price = movie.Price * ((double)order.TaxRate / 100.0 + 1.0);

        var checkout = await CheckoutSession.CreateCustomCheckoutSessionResponse(new CreateCustomCheckoutSessionRequest
        {
            Currency = "USD",
            Name = $"Film: {movie.Title}",
            Price = Convert.ToInt64(price),
            Quantity = 1,
            OrderId = order.Id
        });
        
        Response.Headers.Add("Location", checkout.RedirectUrl);
        return new StatusCodeResult(303);
    }

    [HttpGet("/order/process/success")]
    public async Task<IActionResult> Success()
    {
        return View();
    }

    [HttpGet("/order/process/error")]
    public async Task<IActionResult> Error()
    {
        return View();
    }

    [HttpPost("/order/webhook")]
    public async Task<IActionResult> WebHook()
    {
        var json = await new StreamReader(HttpContext.Request.Body).ReadToEndAsync();
        
        var stripeEvent = EventUtility.ConstructEvent(json,
            Request.Headers["Stripe-Signature"], "whsec_5c6c30132254d32b19b74dda9080e6138de4a745217f08897d98b26e5c2b4dd3");

        if (stripeEvent.Type == Events.CheckoutSessionCompleted)
        {
            var orderId = (stripeEvent.Data.Object as Session)?.ClientReferenceId ?? "";

            if (!string.IsNullOrWhiteSpace(orderId))
            {
                var order = await _orderService.Get(Guid.Parse(orderId));

                order!.Status = OrderStatus.Completed;

                await _orderService.Update(order);
            }
        }

        return Ok();
    }
}